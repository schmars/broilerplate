# encoding: utf-8

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'broilerplate/version'

Gem::Specification.new do |spec|
  spec.name          = 'broilerplate'
  spec.version       = Broilerplate::VERSION
  spec.authors       = ['Lars Gierth']
  spec.email         = ['larsg@systemli.org']
  spec.description   = %q{My personal Ruby boilerplate}
  spec.summary       = %q{My personal boilerplate for Ruby libraries. Fork and code ahead.}
  spec.homepage      = 'https://github.com/lgierth/boilerplate'
  spec.license       = 'Public Domain'

  spec.files         = `git ls-files`.split($/)
  spec.test_files    = spec.files.grep(%r{^spec/})
  spec.require_paths = ['lib']
  spec.executables   = spec.files.grep(%r{^bin/})

  spec.add_development_dependency 'rspec'
end
