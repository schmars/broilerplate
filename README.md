# broilerplate [![Build Status](https://travis-ci.org/lgierth/broilerplate.png?branch=master)](https://travis-ci.org/lgierth/broilerplate) [![Code Climate](https://codeclimate.com/github/lgierth/broilerplate.png)](https://codeclimate.com/github/lgierth/broilerplate) [![Coverage Status](https://coveralls.io/repos/lgierth/broilerplate/badge.png?branch=master)](https://coveralls.io/r/lgierth/broilerplate?branch=master)

If this isn't the repository "broilerplate", chances are the authors of this
repository didn't add a proper README yet and left the boilerplate (me) here.

My personal broilerplate for Ruby libraries. Fork and code ahead.

## Usage

Fork and clone, then execute:

    $ bundle

To verify that everything works, run the CI task, and the included example.

    $ bundle exec rake ci
    $ bundle exec ruby examples/getting_started.rb

Now replace all occurences of `[Bb]roilerplate` with your library's name.
Also replace the metadata in the gemspec.

You also want to enable the Travis and Rdocinfo hooks for your GitHub repository.

## Unlicense

broilerplate is free and unencumbered public domain software. For more
information, see [unlicense.org](http://unlicense.org/) or the accompanying
UNLICENSE file.

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
